/*
 * #######################################################################################
 * ##    Filename:  timedtargetbraking.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "timedtargetbraking.h"

TimedTargetBraking::TimedTargetBraking(std::string param) :
        TimedBehavior(param)
{
    p_speed = std::stof(p_param);

}

void TimedTargetBraking::init()
{
    /*
     * TODO Aufgabe 2.1
     *      Beschleunigen Sie auf <p_speed>
     *      und berechnen Sie die kritische Distanz <p_dist>, berücksichtigen Sie dabei
     *       - Bremsweg
     *       - Reaktionsweg (Wie Aufgabe 2.4, berücksichtigen Sie zusätzlich die Task-Frequenz)
     *       - Radius (Sensorwert ist auf Mitte bezogen)
     */
    // vvvv--
    // ^^^^--
}

void TimedTargetBraking::task() // 100 Hz
{
    /*
     * TODO Aufgabe 2.1
     *      Bremsen Sie, wenn die freie Distanz nach vorne weniger als die kritische Distanz <p_dist> ist.
     *      Beachten Sie, dass dieser Funktion nicht blockieren darf (keine while-Schleife!),
     *      da dieser Task mit 100 Hz immer wieder aufgerufen wird.
     */
    // vvvv--
    // ^^^^--
}

void TimedTargetBraking::finalize()
{
    // beim Beenden: Roboter anhalten.
    p_robot->speed(.0);
}
