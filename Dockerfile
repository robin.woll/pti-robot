FROM ubuntu:20.04

WORKDIR robot

COPY common /common
COPY robot /robot

RUN apt-get update && apt-get install -y \
    make g++

RUN make