/*
 * #######################################################################################
 * ##    Filename:  robot.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "robot.h"
#include "../common/common.h"
#include "cli.h"

#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <chrono>
#include <iostream>

Robot::Robot(CLI* p_cli)
    : p_port(COMMON_ROBOT_PORT),
    p_hostname("localhost")
{
    p_id = rand();
    this->p_cli = p_cli;
    ptrSet = true;
}

Robot::~Robot(){
    disconnectFromServer();
}

void Robot::connectToServer(std::string hostname, int port = -1)
{
    p_hostname = hostname;
    if (port != -1) p_port = port;

    int ret = tcpSocket.connectToHost(p_hostname.c_str(), p_port);
    if(ret) p_connected();

}

void Robot::disconnectFromServer()
{
    tcpSocket.sendString("EXIT\n");
    tcpSocket.disconnectFromHost();
}

std::string Robot::suspend()
{
    std::string cmd = "SUSPEND";
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        return answer.substr(3,answer.length()); // return value after delimiter ":"
    }
    return "";
}

std::string Robot::reconnect(std::string idkey)
{
    std::string cmd = "RECONNECT:" + idkey;
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        return answer.substr(3,answer.length()); // return value after delimiter ":"
    }
    return "";
}

void Robot::reset()
{
    tcpSocket.dialog("RESET!");
}

bool Robot::isConnected()
{
    //return (tcpSocket.state() == QAbstractSocket::ConnectedState);
    //FIXME: this is of course not always true...
    return true;
}

void Robot::p_connected()
{
    std::string cmd = "ID:" + std::to_string(p_id);
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"

    } else {
        /* add to log */
    }
    /* setName() wird von MainWindow::connected() aufgerufen */
}

void Robot::p_disconnected()
{
}

/*
 * Diese Funktion gibt den Radius des Roboters zurück.
 * Sie ist nützlich, um den Abstand (der immer auf die Mitte des Roboters bezogen ist)
 * flexibel zu berücksichtigen.
 */
float Robot::radius()
{
    return 1.0; /* fixed for now */
}

/*
 * Diese Funktion gibt die Beschleunigung der Arena zurück.
 * Der Wert 4.0 ist eine vorläufige Näherung.
 * Der exakte Wert wird in Aufgabe 1.4 gemessen und kann dann hier korrigiert werden.
 */
float Robot::accel()
{
    /*
     * TODO Aufgabe 1.4
     *      Tragen Sie hier den gemessenen Wert der Beschleunigung ein.
     */

    return 4.0; /* Achtung: Test-Wert; gemessenen Wert eintragen! */
}

/*
 * Diese Funktion dient als Interface der dialog() Funktion des TCPSockets.
 * Sie sendet einen String als Nachricht an den Server und gibt dessen Antwort zurück.
 */
std::string Robot::itfDialog(std::string message){
    return tcpSocket.dialog(message);
}

/*
 * Diese Funktion sendet den Befehl zum Setzen des Namens an den Roboter
 */
void Robot::sendName(std::string name)
{
    std::string cmd = "NAME:" + name;
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,2).compare("ok") == 0){  // if answer starts with "ok"
        p_name = name; // set name
    }
}

/*******************************************************************
 *
 * void Robot::speed(float value)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   float value         neue Soll-Geschwindigkeit (m/s)
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion setzt eine neue Sollgeschwindigkeit.
 * Die effektive Geschwindigkeit wird mit der konstanten Beschleunigung a
 * der Arena erreicht. Der Bremsweg s_B aus effektiver Geschwindigkeit v
 * beträgt s_B = v^2 / 2a
 *******************************************************************/
void Robot::speed(float value)
{
    std::string cmd = "SPEED:" + std::to_string(value);
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,2).compare("ok") == 0){  // if answer starts with "ok:"
        /* no error */
    }
}

/*******************************************************************
 *
 * void Robot::turn(float angle)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   float angle     Drehe um <angle> Grad (-360..360)
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion sendet den Abbiegebefehl an den Server.
 * Sensorabfragen sind ab sofort auf die neue Ausrichtung bezogen,
 * während die effektive Fahrtrichtung abhängig von der Arena-Beschleunigung
 * nachgeführt wird (es wird in einem Radius r=v^2/2a(=s_B) abgebogen)
 * (r:Radius, v:Geschwindigkeit, a:Beschleunigung, s_B:Bremsweg)
 *******************************************************************/
void Robot::turn(float angle)
{
    std::string cmd = "TURN:" + std::to_string(angle);
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,2).compare("ok") == 0){  // if answer starts with "ok:"
        /* no error */
    }
}


/*******************************************************************
 *
 * float Robot::getSpeed(unsigned int msec)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   unsigned int msec   Beobachtungsintervall
 * Rückgabewert:
 *   float               gemessene Geschwindigkeit (m/s)
 *------------------------------------------------------------------
 * Diese Funktion nutzt den Wegstreckenzähler distance(), um die
 * während <msec> Millisekunden gefahrene Wegstrecke in die
 * Geschwindigkeit umzuwandeln.
 * Dabei blockiert diese Funktion die angegebenen Millisekunden,
 * so dass das Programm so lange nicht reagiert!
 *******************************************************************/
float Robot::getSpeed(unsigned int msec)
{
    /*
     * TODO Aufgabe 1.1
     *      Implementieren Sie diese Funktion
     *       - Wegstreckenzähler auslesen
     *       - <msec> Millisekunden warten (usleep())
     *       - Wegstreckenzähler nochmal auslesen
     *       - Geschwindigkeit berechnen und zurück geben.
     */

    // vvvv--
    // berechne gefahrenen Weg und berechne daraus die Geschwindigkeit.
    // ^^^^--

    return 42.0;        // Achtung: Test-Wert, muss durch berechneten Wert ersetzt werden!
}

/*******************************************************************
 *
 * float Robot::getSpeedAsync()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   float               gemessene Geschwindigkeit (m/s)
 *------------------------------------------------------------------
 * Im Gegensatz zu getSpeed() blockiert diese Funktion nicht, sondern
 * berechnet die aktuelle (Durchschnitts-)Geschwindigkeit seit dem
 * letzten Aufruf. Diese Funktion kehrt also sofort zurück und ist
 * geeignet, wenn das Programm weiter reagieren muss.
 * Wenn jedoch der Wegstreckenzähler zurück gesetzt wird (distanceReset()),
 * kann diese Funktion falsche Werte liefern!
 *******************************************************************/
float Robot::getSpeedAsync()
{
    /*
     * TODO Aufgabe 2.2
     *      Berechnen Sie die Geschwindigkeit seit dem letzten Aufruf dieser Funktion.
     *      Diese Funktion soll *nicht* blockieren (msleep() etc.).
     */


    static std::chrono::time_point<std::chrono::high_resolution_clock> t_last;               // statisch: letzte Zeit
    static float d_last = 0.0;            // statisch: letzte Distanz
    std::chrono::time_point<std::chrono::high_resolution_clock> t_now;
    float d_now;
    static float v = 0.0;                 // statisch: letzte berechnet Geschw.

    // ermittle aktuelle Werte (t_now, d_now)
    // vvvv--
    // ^^^^--

    if (d_last == 0) {
        // erster Durchlauf (d_last noch 0)
        // speichere aktuelle Werte für nächsten Aufruf
        t_last = t_now;
        d_last = d_now;
    } else {
        int milliseconds = std::chrono::duration_cast<std::chrono::milliseconds>(t_now - t_last).count();
        if ( milliseconds > 100){
            // nur, wenn letzter Aufruf > 100ms zurück liegt
            // berechne Geschwindigkeit (v)

            // vvvv--
            // ^^^^--

            // speichere aktuelle Werte für nächsten Aufruf
            t_last = t_now;
            d_last = d_now;
        }
    }

    return v;   // static; gibt evtl. letzten bekannten Wert zurück
}


/*******************************************************************
 *
 * void Robot::message(std::string msg)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   std::string msg         Nachricht
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion gibt die angegebene Zeichenkette auf der Konsole
 * aus. Sie dient zur Ausgabe von Debugmeldungen und zur
 * Anzeige von gemessenen Ergebnissen.
 *******************************************************************/
void Robot::message(std::string message)
{
    p_cli->logString(">> Robot: " + message);
}

/*******************************************************************
 *
 * void Robot::say(std::string msg)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   std::string msg         Nachricht
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion sendet die Nachricht an den Server, der sie in
 * die globalen Logbücher der Observer einträgt. Das ist weniger
 * für Chats gedacht als eher zur Ausgabe von Werten, die nur der
 * Server kennt. Dazu werden einige Platzhalter erkannt:
 *    $(P)  : Position (x,y) des Roboters
 *    $(X)  : zurückgelegte Distanz nur in X-Richtung seit distanceReset()
 *    $(Y)  : ebenso für Y-Richtung
 *    Die letzten beiden können um eine Konstante reduziert werden:
 *    %(X-4.5) :
 * In der Nachricht werden diese Platzhalter durch ihre Werte
 * ersetzt und an die Observer geschickt.
 *******************************************************************/
void Robot::say(std::string msg)
{
    std::string cmd = "SAY:" + msg;
    tcpSocket.dialog(cmd);
}

/* intern:
 * wird über Timer regelmäßig aufgerufen, um zu prüfen, ob es Nachrichten für das lokale Logbuch gibt
 */
void Robot::check_message()
{
    std::string cmd = "MESSAGE?";
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        /* no error */
        std::cout << "Message: " << answer.substr(3,answer.length()) << std::endl;
    }
}


/*******************************************************************
 *
 * void Robot::distanceReset()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion setzt den internen Wegstreckenzähler auf Null zurück.
 *******************************************************************/
void Robot::distanceReset()
{
    std::string cmd = "DISTANCE:RESET";
    std::string answer = tcpSocket.dialog(cmd);
    if(answer.substr(0,2).compare("ok") == 0){  // if answer starts with "ok:"
        /* no error */
        p_distance = .0;
    }

}

/*******************************************************************
 *
 * float Robot::distance()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   float              aktueller Wert des Wegstreckenzählers
 *------------------------------------------------------------------
 * Diese Funktion fragt den aktuellen Wert des Wegstreckenzählers
 * ab und gibt diesen zurück.
 *******************************************************************/
const float F_NAN = .0/.0;  /* there is no such symbol in Qt */
float Robot::distance()
{
    std::string cmd = "DISTANCE?";
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        /* no error */
        p_distance = std::stof(answer.substr(3,answer.length()));
    } else p_distance = F_NAN;          // used as "unknown"

    return p_distance;
}



/*******************************************************************
 *
 * float Robot::lineSensor(float angle)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   float angle        Winkel relativ zur Blickrichtung
 * Rückgabewert:
 *   float              Distanz bis Hindernis
 *------------------------------------------------------------------
 * Diese Funktion bestimmt die Distanz bis zum nächsten Hindernis
 * im angegebenen Winkel relativ zur Blickrichtung. Die Distanz
 * ist auf den Mittelpunkt des Roboters bezogen, es muss also
 * Robot::radius() abgezogen werden, um eine Kollision zu vermeiden.
 *******************************************************************/
float Robot::lineSensor(float angle)
{
    // Member-Variablen
    p_line_distance = 42.0;   // speichert zuletzt abgefragten Wert (hier: mit nonsense-Wert initialisiert)
    p_line_angle = angle;     // dito (Winkel)

    /*
     * TODO Aufgabe 0.2
     *      Sensorwert für <angle> abfragen und in p_line_distance speichern
     *      Nutzen Sie in dieser Funktion dialog() für die Kommunikation mit dem Server.
     *      (Später soll dann speed(), turn(), lineSensor() etc. zur Kontrolle des Roboters verwendet werden.)
     *      Hinweis: siehe benachbarte Funktionen...
     */


    // vvvv--
    // ^^^^--
    std::string cmd = ("LINE?"+str(angle));
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        /* no error */
        p_line_distance = std::stof(answer.substr(3,answer.length()));
    } else p_line_distance = F_NAN;          // used as "unknown"

    return p_line_distance;                                // gebe Antwort zurück
}



/*******************************************************************
 *
 * float Robot::sectorSensor(int nbr)
 *
 *------------------------------------------------------------------
 * Parameter:
 *   int nbr            Nummer des Sektors (-2..2)
 * Rückgabewert:
 *   float              Distanz bis Hindernis
 *------------------------------------------------------------------
 * Diese Funktion bestimmt die Distanz bis zum nächsten Hindernis
 * im angegebenen Sektor relativ zur Blickrichtung. Die Distanz
 * ist auf den Mittelpunkt des Roboters bezogen, es muss also
 * Robot::radius() abgezogen werden, um eine Kollision zu vermeiden.
 *******************************************************************/
float Robot::sectorSensor(int nbr)
{
    std::string cmd = "SECTOR?" + std::to_string(nbr);
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"
        /* no error */
        p_sector_distance[nbr+2] = std::stof(answer.substr(3,answer.length()));
    }

    return p_sector_distance[nbr+2];
}

/*******************************************************************
 *
 * float Robot::radarSensor()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   int nbr            Nummer des Sektors (-2..2)
 * Rückgabewert:
 *   int                Gesamtzahl an Coins
 *   int &count         Gesamtzahl an Coins
 *------------------------------------------------------------------
 * ...
 *******************************************************************/

// FIXME: Currently not needed as items are not supported
// QList<radar_obj_t> const *Robot::radarSensor(int &count)
// {
//     count = -1;
//     std::string cmd = std::string("RADAR?\n");
//     std::string answer = dialog(cmd);
//     if (answer.startsWith("ok:")) {
//         /* no error */
//         p_radar_results.clear();
//         count = answer.section(':', 1, 1).toInt();
//         if (count > 0) {
//             for (int i=0; i<count && i < 10; i++) {
//                 std::string r = answer.section(':', 2+i, 2+i);
//                 p_radar_results.push_back((radar_obj_t){r.section(',', 0, 0).toFloat(), r.section(',', 1, 1).toFloat()});
//             }
//         }
//         emit got_radar(count, (const QList<radar_obj_t>)p_radar_results);
//     }
//     return NULL;// TODO : this gives an Error: &((const QList<radar_obj_t>)p_radar_results);
// }


void Robot::setName(std::string name)
{
    sendName(name);
}

std::string Robot::name()
{
    return p_name;
}


/*
 * nur zum internen Gebrauch:
 * fragt lineSensor für alle 360 Grad ab
 * (wird von GUI für Auto-Sensor benötigt
 */
void Robot::lineSensorAll()
{
    std::string cmd = "LINE_ALL?";
    std::string answer = tcpSocket.dialog(cmd);

    if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"

        /* no error */
        std::vector<float> vDist(360,0.0);

        answer.erase(answer.begin(),answer.begin() + answer.find(":") + 1); // remove "ok:"

        for (auto it = vDist.begin(); it != vDist.end(); ++it) {
            p_line_distance = std::stof(answer.substr(0,answer.find(":")));
            p_line_angle = it - vDist.begin();

            *it = p_line_distance;
            answer.erase(answer.begin(), answer.begin() + answer.find(":") + 1); // remove "<float>:"
        }
    }
}

