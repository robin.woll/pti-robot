/*
 * #######################################################################################
 * ##    Filename:  followwall.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "followwall.h"

FollowWall::FollowWall(std::string param) :
        TimedBehavior(param)
{
    p_speed = std::stof(p_param);

}

void FollowWall::init()
{
    /*
     * TODO Aufgabe 2.3
     *      Beschleunigen Sie auf <p_speed>
     *      und berechnen Sie die kritische Distanz <p_dist>, falls Sie diese im Task benötigen
     */
    // vvvv--
    // ^^^^--
}

void FollowWall::task() // 100 Hz
{
    /*
     * TODO Aufgabe 2.3
     *      Regeln Sie die Fahrt des Roboters so, dass er linker Hand an der Wand entlang fährt.
     */
    // vvvv--
    // ^^^^--
}

void FollowWall::finalize()
{
    p_robot->speed(.0);
}
