/*
 * #######################################################################################
 * ##    Filename:  targetbraking.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */

#include "targetbraking.h"

/*******************************************************************
 *
 * TargetBraking::TargetBraking()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   QString param       Zeichenkette mit Parametern (s.u.)
 *------------------------------------------------------------------
 * Der Konstruktor initialisiert interne Variablen mit Werten,
 * die als Zeichenkette param übergeben werden können.
 * Der Funktionsprototyp für diese Zeichenkette ist:
 *           f(float speed = 4.0)
 * Es gibt also einen optionalen Parameter mit Standardwert 4.0,
 * der die Geschwindigkeit angibt.
 *******************************************************************/
TargetBraking::TargetBraking(std::string param) :
    Behavior(param)
{
    p_speed = std::stof(p_param);
}

/*******************************************************************
 *
 * void TargetBraking::start()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Die Startmethode wird beim Aktivieren dieses Verhaltens aufgerufen.
 * So lange diese Methode läuft, wird das Programm nicht auf Eingaben
 * reagieren.
 *******************************************************************/
void TargetBraking::start()
{
    /*
     * TODO Aufgabe 1.5
     *      Geben Sie einen Bremsbefehl, so dass der Roboter möglichst nah vor der Wand zum Stehen kommt
     *      (ohne die Wand zu berühren).
     *      Die Geschwindigkeit ist in der Member-Variablen p_speed gespeichert.
     */


    float critical_distance;    // Distanz, bei der gebremst werden muss.

    // Berechnen Sie critical_distance, berücksichtigen Sie dabei:
    //   - Bremsweg
    //   - Reaktionsweg (Netzwerklatenz)
    //   - Radius (Sensorwert ist auf Mitte bezogen)
    // vvvv--
    // ^^^^--

    // Beschleunige auf p_speed
    // vvvv--
    // ^^^^--

    // Fahre, bis kritische Distanz erreicht
    // vvvv--
    // ^^^^--

    // Bremsen!
    // vvvv--
    // ^^^^--

    // Warte, bis Roboter steht
    // vvvv--
    // ^^^^--

    // Ausgabe
    // vvvv--
    // ^^^^--

    // drehe für nächste Fahrt
    p_robot->turn(180);
}

/*******************************************************************
 *
 * TargetBraking::stop()
 *
 *------------------------------------------------------------------
 * Parameter:
 *   none
 * Rückgabewert:
 *   none
 *------------------------------------------------------------------
 * Diese Funktion für das Beenden des Verhaltens wird nicht benötigt.
 *******************************************************************/
void TargetBraking::stop()
{
    // nothing
}
