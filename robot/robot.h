/*
 * #######################################################################################
 * ##    Filename:  robot.h
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#ifndef ROBOT_H
#define ROBOT_H

#include<stdio.h>
#include<vector>
#include<list>

#include "tcpSocket.h"

typedef struct radar_object {
    float dist;
    float dir;
} radar_obj_t;

class CLI;
class Robot
{
public:
    //Robot();
    Robot(CLI* p_cli);
    ~Robot();
    void connectToServer(std::string, int);
    void disconnectFromServer();
    std::string suspend(void);
    std::string reconnect(std::string idkey);
    void reset();
    bool isConnected();
    void setName(std::string);
    std::string name();

    void turn(float);
    void speed(float);
    float distance();
    void distanceReset();
    float lineSensor(float);
    void lineSensorAll();
    float sectorSensor(int);
    void message(std::string message = "");
    void say(std::string);
    float getSpeed(unsigned int msec = 500);
    float getSpeedAsync();

    float radius();
    float accel();

    void check_message();

    void connected();
    void disconnected();
    void got_message(std::string);
    void got_distance(float);
    void got_lineDistance(float, float);
    void got_lineDistanceAll(std::vector<float> &);
    void got_sectorDistance(int, float);
    void got_radar(int, std::list<radar_obj_t> const &);

    std::string itfDialog(std::string message);

private:
    int p_port;
    int p_id;
    std::string p_hostname;
    bool ptrSet = false;

    TcpSocket tcpSocket;
    CLI* p_cli;

    std::string p_name;
    float p_distance;
    float p_line_angle;
    float p_line_distance;
    float p_sector_distance[5];
    
    std::list<radar_obj_t> p_radar_results;

    void sendName(std::string);

    void p_connected();
    void p_disconnected();


};

#endif // ROBOT_H
