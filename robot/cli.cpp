/*
 * #######################################################################################
 * ##    Filename:  cli.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##      Author:  Felix Wege (ACS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##
 * #######################################################################################
 */

#include <iostream>
#include <regex>
#include <stdlib.h>
#include <unistd.h>
#include <chrono>

#include "cli.h"
#include "robot.h"
#include "behavingrobot.h"

#include "braketest.h"
#include "turntest.h"
#include "targetbraking.h"

#include "timedtargetbraking.h"
#include "timedbrakecontrol.h"
#include "followwall.h"
#include "speedcontrol.h"

#include <signal.h>

#define DEFAULTLOGSIZE 5

int sigint = 0;//1 führt zum Beenden der Funktion startCLI

//signal_handler zum Abfangen von STRG-C
void signal_handler(int signum){
    sigint = 1;
    std::cout <<std::endl << "Please shut down the program with the command exit next time. Press enter to exit." << std::endl;
    //Beenden durch setzen der Variablen sigint, die in startCLI abgefragt wird
}

CLI::CLI(){
    // Constructor
    running = true;
    LOGSIZE = DEFAULTLOGSIZE;
    init();
    p_robot = new BehavingRobot(this);
    p_robot->connectToServer(serverAddress, serverPort);
    p_robot->setName(robotName);
}

CLI::CLI(std::string _robotName, std::string _serverAddress, int _serverPort){
    running = true;
    LOGSIZE = DEFAULTLOGSIZE;
    if(_robotName.length() < 4){
        std::cout << "ERROR: name must have at least 4 characters..." << std::endl;
        return;
    }
    if(_serverAddress.empty()){
        std::cout << "ERROR: no server address given..." << std::endl;
        return;
    }

    robotName = _robotName;
    serverAddress = _serverAddress;
    serverPort = _serverPort;
    p_robot = new BehavingRobot(this);
    p_robot->connectToServer(serverAddress, serverPort);
    p_robot->setName(robotName);
}

CLI::~CLI(){
    // Destructor
    delete p_robot;
}

void CLI::init() {
    std::string _robotName = "";
    std::string _serverAddress = "";
    int _serverPort = 1231231;

    do // Read robot name
    {
        std::cout << "Enter robot name: " << std::endl;
        std::cin >> _robotName;

        if(_robotName.length() < 4) std::cout << "ERROR: name must have at least 4 characters! Try again..." << std::endl;

    } while (_robotName.length() < 4);


    do // Read address of arena server
    {
        std::cout << "Enter server address [XX.XX.XX.XX]: " << std::endl;
        std::cin >> _serverAddress;

    } while (_serverAddress.empty());

    do // Read address of arena server
    {
        std::cout << "Enter server port: " << std::endl;
        std::cin >> _serverPort;

    } while (_serverPort == 123123123123);

    robotName = _robotName;
    serverAddress = _serverAddress;
    serverPort = _serverPort;
}

void CLI::startCLI() {
    //Abfangen von STRG-C Signal zum sicheren Beenden des Programms
    signal(SIGINT, signal_handler);

    while(running){
        // update scene
        clearScene();
        printScene(commands);

        // read new input
        std::string input;
        input = getUserInput();
        if(input == "exit" || sigint == 1){
            break;
        } else {
            processInput(input);
        }
    }
}

void CLI::printScene(std::string fixedMessage/* = "" */) {

    std::string sp[7] = {"  ","  ","  ","  ","  ","  ","  "};
    float interval = maxSpeed/7 - .1; //Make interval a little less so that upper bound can be hit
    unsigned int index = 0;
    if(speedVal!=0.0) index = abs(speedVal/interval);

    for(unsigned int i = 0; i < index; ++i ){
        sp[i] = "##";
    }

    std::string space = "    "; // 4 spaces
    if(speedVal >= 10.0){
        // remove one space for double digit
        space.resize(space.size()-1);
    } else if (speedVal < 0.0) {
        // remove one space for "-"
        space.resize(space.size()-1);
    } else if (speedVal <= -10.0) {
        // remove two spaces for "-" and double digit
        space.resize(space.size()-2);
    }

    std::cout << "#################################################################################"  << std::endl;
    std::cout << "PTI - V2 : RobotCLI     Name: " << robotName << "       Server: "<< serverAddress << ":" << serverPort <<std::endl;
    std::cout << "_________________________________________________________________________________"  << std::endl;
    std::cout << "" << std::endl;
    std::cout << "  Target Speed:                                                    \\  /          "  << std::endl;
    std::cout << space << speedVal <<" / " << maxSpeed << "                 Manual Control Mapping                 ------         "  << std::endl;
    std::cout << "      __                  ------------------------               | O  O |        "  << std::endl;
    std::cout << "     |"<<sp[6]<<"|                      w = Speed Up                      | +==+ |        "  << std::endl;
    std::cout << "     |"<<sp[5]<<"|                      s = Speed Down               (_)  | °°°° |  (_)   "  << std::endl;
    std::cout << "     |"<<sp[4]<<"|                      a = Turn Left                  \\  --------  /    "  << std::endl;
    std::cout << "     |"<<sp[3]<<"|                      d = Turn Right                  \\/        \\/   "  << std::endl;
    std::cout << "     |"<<sp[2]<<"|                                                      /          \\    "  << std::endl;
    std::cout << "     |"<<sp[1]<<"|                 setspeed = Set Target Speed          -------------     "  << std::endl;
    std::cout << "     |"<<sp[0]<<"|                    reset = Reset Robot               OO vvvvvvv OO      "  << std::endl;
    std::cout << "                                                               OO         OO      "  << std::endl;
    std::cout << "________________________________________________________________________________   "  << std::endl;
    if(fixedMessage != ""){
        std::cout << fixedMessage << std::endl;
        std::cout << "________________________________________________________________________________   "  << std::endl;
    }
    std::cout << "" << std::endl;
    std::cout << " < Log: >                                                                " << std::endl;
    std::cout << "" << std::endl;
    for (auto it = log.begin() ; it != log.end();  ++it){
        std::cout << *it << std::endl;
    }
    std::cout << "________________________________________________________________________________"  << std::endl;

}

void CLI::setLogSize(unsigned int size){
    LOGSIZE = size;
}

void CLI::logString(std::string message){
    log.push_back(message);

    while(log.size() > LOGSIZE){
        log.erase(log.begin());
    }
}

void CLI::clearScene() {
    system("clear");
}

void CLI::updateScene(std::string fixedMessage/* = "" */){
    clearScene();
    printScene(fixedMessage);
}

/************************
* First level functions *
************************/

void CLI::telnet(){

    std::string fixedMessage = "Emulating telnet connection... All inputs are directly send to robot. \n Use \"exit\" to leave.";
    updateScene(fixedMessage);

    std::string input = "";

    while(true){
        input = getUserInput();
        if(input == "exit") break;
        logString(input);
        logString(p_robot->itfDialog(input));
        updateScene(fixedMessage);
    }

    p_robot->reset();
}

void CLI::sensors(){
    std::string fixedMessage = std::string("\n | Sensors: |\n\n")
                             + std::string(" distance  --> get travelled distance     | rDistance  --> reset distance sensor\n")
                             + std::string(" line      --> get line sensor for ???    | aLine      --> get all line sensor readings\n")
                             + std::string(" sector    --> get sector sensor readings | speed      --> get current speed\n\n")
                             + std::string(" exit      --> go back to 'commands' ");
    updateScene(fixedMessage);

    std::string input = "";
    while(true){
        input = getUserInput();
        if(input == "exit") break;
        processInput(input);
        updateScene(fixedMessage);
    }
}

void CLI::commSeq(){
    std::string fixedMessage = std::string("\n | Command Sequences: |\n\n")
                             + std::string(" simple      --> send simple command sequence\n")
                             + std::string(" measureLat  --> measure latencies\n\n")
                             + std::string(" exit        --> go back to 'commands' ");
    updateScene(fixedMessage);

    std::string input = "";
    while(true){
        input = getUserInput();
        if(input == "exit") break;
        processInput(input);
        updateScene(fixedMessage);
    }
}

void CLI::behavior(){
    std::string fixedMessage = std::string("\n | Behavior: |\n\n")
                             + std::string(" brakeTest      --> start BrakeTest behavior\n")
                             + std::string(" turnTest       --> start TurnTest behavior\n")
                             + std::string(" targetBraking  --> start TargetBreaking behavior\n\n")
                             + std::string(" exit           --> go back to 'commands' ");
    updateScene(fixedMessage);

    std::string input = "";
    while(true){
        input = getUserInput();
        if(input == "exit") break;
        processInput(input);
        updateScene(fixedMessage);
    }
}

void CLI::tBehavior(){
    std::string fixedMessage = std::string("\n | Timed Behaviors: |\n\n")
                             + std::string(" tTargetBraking  --> start TimedTargetBraking behavior\n")
                             + std::string(" tBrakeControl   --> start TimedBrakeControl behavior\n")
                             + std::string(" followWall      --> start FollowWall behavior\n")
                             + std::string(" speedControl    --> start SpeedControl behavior\n")
                             + std::string(" tStop           --> stops current behavior\n\n")
                             + std::string(" exit            --> go back to 'commands' ");
    updateScene(fixedMessage);

    std::string input = "";
    while(true){
        input = getUserInput();
        if(input == "exit") break;
        processInput(input);
        updateScene(fixedMessage);
    }
}

/*************************
* Second level functions *
**************************/
//// sensors
float CLI::distance(){
    return(p_robot->distance());
}

void CLI::rDistance(){
    p_robot->distanceReset();
}

float CLI::line(double angle){
    return(p_robot->lineSensor(angle));
}

void CLI::aLine(){
    logString("Enetered aLine()...");
}

float CLI::sector(int sec){
    return(p_robot->sectorSensor(sec));
}

float CLI::speed(){
    /*
     * TODO Aufgabe 1.1
     *      Testen Sie Robot::getSpeed() indem sie "speed" im CLI aufrufen
     * TODO Aufgabe 2.2
     *      Wenn Sie Robot::getSpeedAsync() implementiert haben,
     *      rufen Sie statt der ersten die zweite Zeile auf (die getSpeedAsync verwendet).
     */

    return(p_robot->getSpeed(100));
    //return(p_robot->getSpeedAsync());
}

//// commSeq
void CLI::simple(){
   /*
     * TODO Aufgabe 0.3
     *      Implementieren Sie eine Sequenz von Aktionen...
     *      Zeiger auf class Robot: p_robot
     */
    p_robot->distanceReset():
    p_robot->speed(7):
    sleep(2)
    p_robot->turn(90):
    p_robot->speed(0):
    logString("Wegstrecke" + std::to_string(p_robot->distance())+ " m")

    // vvvv--
    // ^^^^--

    float dist1 = 42.0; // TODO: Wegstreckenzähler auslesen (dist1 wird unten ausgegeben)
    // vvvv--
    // ^^^^--
    logString("Distance before stop: " + std::to_string(dist1) + " m");


    /*
     * TODO Aufgabe 1.1
     *      warten, bis Roboter steht und Wegstreckenzähler erneut ausgeben.
     */

    // warten, bis Geschwindigkeit 0 erreicht hat.
    // vvvv--
    // ^^^^--

    float dist2 = 42.0; // TODO: Wegstreckenzähler auslesen (dist2 wird unten ausgegeben)
    // vvvv--
    // ^^^^--
    logString("Distance after stop: " + std::to_string(dist2) + " m");

}

void CLI::measureLat(){
    /*
     * Beispiel einer Zeitmessung:
     *      - Erzeuge time_points für Start und Ende
     *      - Speichere aktuelle Zeit in t_start
     *      - Führe Befehl(e) aus
     *      - Speichere aktzelle Zeit in t_end
     *      - Berechne Differenz
     *      - Ausgabe der Differenz in CLI Log
     */

    // Beispiel: Startzeit; 100 mal etwas ausführen (hier: usleep(10e3)), dann Ausgabe es Mittelwerts
    std::chrono::time_point<std::chrono::high_resolution_clock> t_start;
    std::chrono::time_point<std::chrono::high_resolution_clock> t_end;

    t_start = std::chrono::high_resolution_clock::now();
    for (unsigned int i=0; i<100; ++i) {
        usleep(10e3);
    }
    t_end = std::chrono::high_resolution_clock::now();

    int microseconds = std::chrono::duration_cast<std::chrono::microseconds>(t_end - t_start).count()/100;
    logString("Time for usleep(10e3): " + std::to_string(microseconds) + "us");

    /*
     * TODO Aufgabe 1.3
     *      Messen Sie die Latenz einiger Aktionen, insbesondere:
     *       - Befehle (speed(), turn())
     *       - Sensoren (distance(), lineSensor(), sectorSensor())
     */

    // vvvv--
    // ^^^^--
}

void CLI::waitForUser(){
    std::string eternalVoid;
    std::cout << "Press Enter to continue..." << std::endl << std::flush;
    eternalVoid = std::cin.get();
}
//// behavior
void CLI::brakeTest(std::string param){
    p_robot->setBehavior(new BrakeTest(param));
}

void CLI::turnTest(std::string param){
    p_robot->setBehavior(new TurnTest(param));
}
void CLI::targetBraking(std::string param){
    p_robot->setBehavior(new TargetBraking(param));
}
//// tBehavior
void CLI::tTargetBraking(std::string param){
    p_robot->setBehavior(new TimedTargetBraking(param));
}

void CLI::tBrakeControl(std::string param){
    p_robot->setBehavior(new TimedBrakeControl(param));
}

void CLI::followWall(std::string param){
    p_robot->setBehavior(new FollowWall(param));
}

void CLI::speedControl(std::string param){
    p_robot->setBehavior(new SpeedControl(param));
}

void CLI::tStop(){
    p_robot->stop();
}

/*************************
* User Interactions *
**************************/

std::string CLI::getUserInput() {
    /* read user input and return it */
    std::string input;
    std::cout << "> ";
    //std::cin >> input;
    std::getline(std::cin, input);
    return input;
}

void CLI::processInput(std::string input){
    /* if-else for input */
    input.erase(0, input.find_first_not_of(" "));//Vorangestellte Leerzeichen werden entfernt
    input.erase(input.find_last_not_of(" ") + 1);//Hintenangestelle Leerzeichen werden entfernt
    std::string logstring = input;
    std::size_t pos;
    std::string param = "4.0"; // default parameter for behavior

    // log input and update scene
    logString(logstring);
    updateScene("\nPROCESSING " + input + " - CLI BLOCKED!");


    if(input == "exit") {
        running = false;

    } else if(input == "telnet"){
        telnet();

    } else if((pos = input.find("logsize")) == 0) {
        std::string s_size = "";
        int size = -1;

        if(input.size() > 7){ //additional parameter given
            s_size = input.substr(8, input.size()-8);
            try {
                size = std::stoi(s_size);
            }
            catch (const std::invalid_argument& ia) {
                logString("INVALID INPUT ARGUMENT " + s_size + " - <UNSIGNED INT> EXPECTED");
                size = -1;
            }
            while(size <= 0){
                updateScene("\nGive new logsize <unsgined int> (!=0) [Default=5]: ");
                std::cout << "> ";
                std::getline(std::cin, s_size);
                try{
                    size = std::stoi(s_size);
                }
                catch (const std::invalid_argument& ia) {
                    logString("INVALID INPUT ARGUMENT " + s_size + " - <UNSIGNED INT> EXPECTED");
                    size = -1;
                }
            }
            setLogSize(size);
            logstring = "logsize : new size = " + std::to_string(LOGSIZE);
        } else {
            while(size <= 0){
                updateScene("\nGive new logsize <unsgined int> (!=0) [Default=5]: ");
                std::string s_size = "";
                std::cout << "> ";
                std::getline(std::cin, s_size);
                try{
                    size = std::stoi(s_size);
                }
                catch (const std::invalid_argument& ia) {
                    logString("INVALID INPUT ARGUMENT " + s_size + " - <UNSIGNED INT> EXPECTED");
                    size = -1;
                }

            setLogSize(size);
            logstring = "logsize : new size = " + std::to_string(LOGSIZE);
            }
        }
    } else if(input == "w" || input == "W"){
        int newSpeed = speedVal + 1.0;
        if(abs(newSpeed) > maxSpeed){
            newSpeed = maxSpeed;
            logstring += " : MAX SPEED REACHED!";
        } else {
            logstring += " : Target Speed INCREASED by 1.0";

        }
        speedVal = newSpeed;
        p_robot->speed(newSpeed);

    } else if(input == "s" || input == "S"){
        int newSpeed = speedVal - 1.0;
        if(abs(newSpeed) > maxSpeed){
            newSpeed = -maxSpeed;
            std::cout << speedVal << std::endl;
            logstring += " : MAX REVERSE SPEED REACHED!";
        } else {
            logstring += " : Target Speed DECREASED by 1.0";
        }
        speedVal = newSpeed;
        p_robot->speed(newSpeed);

    } else if(input == "a" || input == "A"){
        p_robot->turn(-10.0);
        logstring += " : -10° turn (left)";

    } else if(input == "d" || input == "D") {
        p_robot->turn(10.0);
        logstring += " : +10° turn (right)";

    } else if (input == "RESET" || input == "reset"){
        p_robot->reset();
        logstring += ": robot reset";
    } else if ((pos = input.find("SETSPEED")) == 0 || (pos = input.find("setspeed")) == 0){
        std::string s_newSpeed = "";
        float newSpeed = -42.0;

        if(input.size() > 8){ // additional parameter given
            s_newSpeed = input.substr(9, input.size()-9);
            try{
                newSpeed = std::stof(s_newSpeed);
            }
            catch (const std::invalid_argument& ia) {
                logString("INVALID INPUT ARGUMENT " + s_newSpeed + " - <FLOAT> EXPECTED");
                newSpeed = -42.0;
            }
            while(abs(newSpeed) > maxSpeed){
                updateScene();
                std::string s_newSpeed;
                std::cout << "Target speed [-" << maxSpeed << "," << maxSpeed << "]: ";
                std::getline(std::cin, s_newSpeed);
                try{
                    newSpeed = std::stof(s_newSpeed);
                }
                catch (const std::invalid_argument& ia) {
                    logString("INVALID INPUT ARGUMENT " + s_newSpeed + " - <FLOAT> EXPECTED");
                    newSpeed = -42.0;
                }
            }

        } else {
            while(abs(newSpeed) > maxSpeed){
                updateScene();
                std::string s_newSpeed;
                std::cout << "Target speed [-" << maxSpeed << "," << maxSpeed << "]: ";
                std::getline(std::cin, s_newSpeed);
                try{
                    newSpeed = std::stof(s_newSpeed);
                }
                catch (const std::invalid_argument& ia) {
                    logString("INVALID INPUT ARGUMENT " + s_newSpeed + " - <FLOAT> EXPECTED");
                    newSpeed = -42.0;
                }
            }
        }
        speedVal = newSpeed;
        p_robot->speed(newSpeed);
        logstring = "speed: set to " + std::to_string(speedVal);
    }

    /*
     else if((pos = input.find("brakeTest")) == 0){
        if(input.size() > 9){ // additional parameter given
            param = input.substr(10, input.size()-10);
        }
        logstring += ": set behavior to BrakeTest(speed = " + param + ")";
        brakeTest(param);
    */
    // First level functions
    else if(input == "sensors"){
        sensors();
    } else if(input == "commSeq"){
        commSeq();
    } else if(input == "behavior"){
        behavior();
    } else if(input == "tBehavior"){
        tBehavior();
    }
    // Second level functions
    //// sensor
    else if(input == "distance"){
        logstring += " = " + std::to_string(distance());
    } else if(input == "rDistance"){
        rDistance();
        logstring += ": new distance = 0.000000";
    } else if ((pos = input.find("line")) == 0) {
        std::string s_angle = "";
        double angle = 0;

        if(input.size() > 4){ // additional parameter given
            s_angle = input.substr(5, input.size()-5);
            try{
                angle = std::stod(s_angle);
            }
            catch (const std::invalid_argument& ia) {
                logString("INVALID INPUT ARGUMENT " + s_angle + " - <DOUBLE> EXPECTED");
                angle = 0;
            }
        } else {
            updateScene("\nGive angle <double>: ");
            s_angle = "";
            std::cout << "> ";
            std::getline(std::cin, s_angle);
            try{
                angle = std::stod(s_angle);
            }
            catch (const std::invalid_argument& ia) {
                logString("INVALID INPUT ARGUMENT " + s_angle + " - <DOUBLE> EXPECTED");
                angle = 0;
            }
        }
        logstring = "line(" + std::to_string(angle) + ") = " + std::to_string(line(angle));
    } else if(input == "aLine"){
        //aLine();
        logstring += ": currently not provided... Sorry!";
    } else if((pos = input.find("sector")) == 0) {
        std::string s_sec = "";
        int sec = -42;

        if(input.size() > 6) { // additional parameter given
            s_sec = input.substr(7, input.size()-7);
            try{
                sec = std::stoi(s_sec);
            }
            catch (const std::invalid_argument& ia) {
                logString("INVALID INPUT ARGUMENT " + s_sec + " - <INTEGER> EXPECTED");
                sec = -42;
            }
            if(sec != -2 && sec != -1 && sec != 0 && sec != 1 && sec != 2) {
                while(sec != -2 && sec != -1 && sec != 0 && sec != 1 && sec != 2){
                    updateScene("\nGive sector <int> [-2,2]: ");
                    std::string s_sec;
                    std::cout << "> ";
                    std::getline(std::cin, s_sec);
                    try{
                        sec = std::stoi(s_sec);
                    }
                    catch (const std::invalid_argument& ia) {
                        logString("INVALID INPUT ARGUMENT " + s_sec + " - <INTEGER> EXPECTED");
                        sec = -42;
                    }
                }
            }
        } else {
            while(sec != -2 && sec != -1 && sec != 0 && sec != 1 && sec != 2){
                updateScene("\nGive sector <int> [-2,2]: ");
                std::string s_sec;
                std::cout << "> ";
                std::getline(std::cin, s_sec);
                try{
                    sec = std::stoi(s_sec);
                }
                catch (const std::invalid_argument& ia) {
                    logString("INVALID INPUT ARGUMENT " + s_sec + " - <INTEGER> EXPECTED");
                    sec = -42;
                }
            }
        }
        logstring = "sector(" + std::to_string(sec) + ") = " + std::to_string(sector(sec));
    } else if(input == "speed"){
        logstring += " = " + std::to_string(speed());
    }
    //// commSeq
    else if(input == "simple"){
        simple();
    } else if(input == "measureLat"){
        measureLat();
    }
    //// behavior
    else if((pos = input.find("brakeTest")) == 0){
        if(input.size() > 9){ // additional parameter given
            param = input.substr(10, input.size()-10);
        }
        logstring += ": set behavior to BrakeTest(speed = " + param + ")";
        brakeTest(param);
    } else if((pos = input.find("turnTest")) == 0){
        if(input.size() > 8){
            param = input.substr(9, input.size()-9);
        }
        logstring += ": set behavior to TurnTest(speed = " + param + ")";
        turnTest(param);
    } else if((pos = input.find("targetBraking")) == 0){
        if(input.size() > 13){ // additional parameter given
            param = input.substr(14,input.size()-14);
        }
        logstring += ": set behavior to TargetBraking(speed = " + param + ")";
        targetBraking(param);
    }
    //// tBehavior
    else if((pos = input.find("tTargetBraking")) == 0){
        if(input.size() > 14){
            param = input.substr(15, input.size()-15);
        }
        logstring += ": set behavior to TimedTargetBraking(speed = " + param + ")";
        tTargetBraking(param);
    } else if((pos = input.find("tBrakeControl")) == 0 ){
        if(input.size() > 13){
            param = input.substr(14, input.size()-14);
        }
        logstring += ": set behavior to TimedBrakeControl(speed = " + param + ")";
        tBrakeControl(param);
    } else if((pos = input.find("followWall")) == 0){
        if(input.size() > 10){
            param = input.substr(11, input.size()-11);
        }
        logstring += ": set behavior to FollowWall(speed = " + param + ")";
        followWall(param);
    } else if((pos = input.find("speedControl")) == 0){
        if(input.size() > 12){
            param = input.substr(13, input.size()-13);
        }
        logstring += ": set behavior to SpeedControl(speed = " + param + ")";
        speedControl(param);
    } else if ((pos = input.find("tStop")) == 0){
        logstring += ": behavior stopped";
        tStop();
    }
    // Invalid command
    else {
        logstring += " <-- is no valid command!...";
    }
    // update log message if the command has appended something
    if(logstring != input){
        if(log.size() == 1){
            auto it = log.begin();
            *it = logstring;
            updateScene();
        }
        for(auto it = log.end(); it != log.begin(); --it){
            if(*it == input){
                *it = logstring;
                updateScene();
                break;
            }
        }
    }

}