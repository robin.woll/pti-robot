/*
 * #######################################################################################
 * ##    Filename:  behavingrobot.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020 
 * ##
 * ##     Authors:  Felix Wege (ACS). Georg Wassen (LFBS) 
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##               Lehrstuhl für Betriebssysteme (LFBS), RWTH Aachen
 * ##
 * #######################################################################################
 */
#include "behavingrobot.h"

BehavingRobot::BehavingRobot(CLI* p_cli)
    : p_behavior(NULL),
      Robot(p_cli)
{
}

BehavingRobot::~BehavingRobot()
{
    delete p_behavior;
}

void BehavingRobot::setBehavior(Behavior *value)
{

    if (p_behavior != NULL) {
        p_behavior->stop();
        p_behavior = NULL;
    }

    p_behavior = value;
    p_behavior->setRobot(this);

    p_behavior->start();
}

void BehavingRobot::stop()
{
    if (p_behavior != NULL)
        p_behavior->stop();
}
