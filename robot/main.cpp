/*
 * #######################################################################################
 * ##    Filename:  main.cpp
 * ##
 * ##     Version:  1.0
 * ##        Year:  2020
 * ##
 * ##      Author:  Felix Wege (ACS)
 * ##
 * ##     Company:  Chair for Automation of Complex Power Systems (ACS), RWTH Aachen
 * ##
 * #######################################################################################
 */

//#include <stdio.h>
#include <unistd.h>
#include <iostream>
#include <chrono>
#include <vector>
#include "cli.h"
#include "tcpSocket.h"
#include "robot.h"
#include "behavingrobot.h"
#include "braketest.h"
#include "turntest.h"
#include "targetbraking.h"
#include "timedtargetbraking.h"
#include "timedbrakecontrol.h"
#include "speedcontrol.h"
#include "followwall.h"

int main(int argc,char** argv){

    CLI *cli;

    if(argc >= 4){
        cli = new CLI(argv[1],argv[2],std::stoi(argv[3]));
        cli->startCLI();
    } else {
        cli = new CLI();
        cli->startCLI();
    }
    delete cli;

    // BehavingRobot robot;
    // robot.connectToServer("127.0.0.1", 12303);

    // robot.setName("testRobot");

    /*
    * BrakeTest Test
    */
    // std::cout << "Create and start BrakeTest..." << std::endl;
    // robot.setBehavior(new BrakeTest("7.1"));

    /*
    * TurnTest Test
    */
    // std::cout << "Create and start TurnTest..." << std::endl;
    // robot.setBehavior(new TurnTest("7.1"));

    /*
    * TargetBraking Test
    */
    // std::cout << "Create and start TargetBraking..." << std::endl;
    // robot.setBehavior(new TargetBraking("7.1"));

    /*
    * TimedTargetBraking Test
    */
    // std::cout << "Create and start TimedTargetBraking..." << std::endl;
    // robot.setBehavior(new TimedTargetBraking("7.1"));

    /*
    * TimedBrakeControl Test
    */
    // std::cout << "Create and start TimedBrakeControl..." << std::endl;
    // robot.setBehavior(new TimedBrakeControl("7.1"));

    /*
    * SpeedControl Test
    */
    // std::cout << "Create and start SpeedControl..." << std::endl;
    // robot.setBehavior(new SpeedControl("7.1"));

    /*
    * FollowWall Test
    */
    // std::cout << "Create and start FolloWall..." << std::endl;
    // robot.setBehavior(new FollowWall("7.1"));



    // while(1);

    // robot.speed(3.0);
    // for(unsigned int i = 0; i < 10 ; ++i){
    //     std::cout << "Robot traveled: " << robot.distance() << std::endl;
    //     usleep(1000000);
    // }
    // robot.speed(0.0);

    //robot.disconnectFromServer();
    //robot.getSpeed()

    // // Create tcp socket object and connect it to arena
    // TcpSocket tcpSock("127.0.0.1",12303);
    // tcpSock.connectToHost();

    // std::string input;
    // std::string answer;
    // std::chrono::time_point<std::chrono::high_resolution_clock> start_time;

    // start_time = std::chrono::high_resolution_clock::now();
    // usleep(100000);
    // auto end_time = std::chrono::high_resolution_clock::now();

    // std::cout << "Duration: " << std::chrono::duration_cast<std::chrono::milliseconds>(end_time - start_time).count() << std::endl;
    // //tcpSock.sendString("NAME:testrobot");
    // // Until canceled by user...
    // while(1){
    //     // Parse user input
    //     std::cout << ">"; std::cin >> input;
    //     // If user wishes to exit, inform server and break
    //     if(input=="exit"){
    //         tcpSock.dialog("EXIT");
    //         break;
    //     }
    //     // Send to server and wait for answer...
    //     answer = tcpSock.dialog(input);
    //     if(input=="LINE_ALL?"){
    //          if(answer.substr(0,3).compare("ok:") == 0){  // if answer starts with "ok:"

    //                 std::cout << answer << std::endl;
    //                 std::cout << "has size: " << answer.size() << std::endl;
    //                 float value;
    //                 int index;
    //                 /* no error */
    //                 answer.erase(answer.begin(),answer.begin() + answer.find(":") + 1);
    //                 std::vector<float> vDist(360,0.0);
    //                 for (auto element = vDist.begin(); element != vDist.end(); ++element) {
    //                     value = std::stof(answer.substr(0,answer.find(":")));
    //                     index = element - vDist.begin();

    //                     std::cout << index << " | " << value << std::endl;
    //                     answer.erase(answer.begin(), answer.begin() + answer.find(":") + 1);
    //                     // p_line_distance = answer.section(':', 0, 0).toFloat();
    //                     // answer = answer.section(":", 1);
    //                     // p_line_angle = i;
    //                     // vDist[i] = p_line_distance;
    //                 }
    //                 //p_sector_distance[nbr+2] = std::stof(answer.substr(3,answer.length()));

    //                 // for (auto i=aVector.begin(); i!=aVector.end(); ++i) {
    //                 //     cout << "I am at position: " << i-aVector.begin() << endl;
    //                 //     cout << "contents here is: " << *i << endl;
    //                 // }
    //         }

    //     }
    //     std::cout << answer << std::endl;
    //     if(answer.substr(0,3).compare("ok:") == 0){ //answer starts with "ok:"
    //         std::cout << answer.substr(3,answer.length()-1) << std::endl;
    //     }
    // }
    // tcpSock.disconnectFromHost();
    return 1;
}