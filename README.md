# Praktikum Technische Informatik - Versuch 2<br>Verteilte und Echtzeitfähige Systeme

![](.images/ascii_header.png)

Dieser Versuch führt in die Grundlagen und Programmierung von verteilten und echtzeitfähigen Systemen ein.
Als verteiltes System kommt eine Robotersimulation zum Einsatz:
Beliebig viele Clients können sich mit dem Server verbinden
und dieser simuliert für jeden Client einen Roboter,
die sich alle in einer Arena bewegen.
Die Clients können über das Netzwerk die Sensoren ihres Roboters abfragen und Steuerbefehle schicken.
Am Beispiel einiger Steuerungen werden Grundlagen vermittelt,
die bei echtzeitfähiger Programmierung zu beachten sind.

Dazu wird der Roboter und das dazugehörende Command Line Inteface (CLI) von den Studierenden eigenständig programmiert und
weiterentwickelt. Der benötigte Quellcode, der entsprechend gekennzeichnete Lücken und Aufgabenstellungen enthält, befindet sich in diesem Repository. Außerdem sind eine aktuelle Version des Skriptes und Hinweise, sowie die verwendeten Adressen von Grafischer Oberfläche (Observer) und Arena (Server) im folgenden gegeben.

> **Hinweis:** Lösungsansätze für häufig auftretende Probleme finden Sie unter: [Troubleshooting](#troubleshooting)

## Inhaltsverzeichnis
- [Praktikum Technische Informatik - Versuch 2<br>Verteilte und Echtzeitfähige Systeme](#praktikum-technische-informatik---versuch-2verteilte-und-echtzeitfähige-systeme)
  - [Inhaltsverzeichnis](#inhaltsverzeichnis)
  - [Adressen](#adressen)
  - [Ordner Struktur](#ordner-struktur)
  - [Versuchsvorbereitung](#versuchsvorbereitung)
  - [How-To](#how-to)
    - [Kompilieren/Bauen der Roboter-Anwendung](#kompilierenbauen-der-roboter-anwendung)
    - [Starten der Roboter-Anwendung](#starten-der-roboter-anwendung)
  - [Troubleshooting](#troubleshooting)
    - [Observer zeigt Arena/Roboter nicht korrekt an...](#observer-zeigt-arenaroboter-nicht-korrekt-an)
    - [Roboter kann keine Verbindung zur Arena herstellen...](#roboter-kann-keine-verbindung-zur-arena-herstellen)
    - [Fehler beim ersten Starten der VM...](#fehler-beim-ersten-starten-der-vm)
  - [Kontakt:](#kontakt)

## Adressen

|  Name | Erklärung | Adresse | Port |
|:-:|:-:|:-:|:-:|
|Observer|Grafische Echtzeit-Darstellung der Arena| https://observer.k8s.eonerc.rwth-aachen.de/?wss:arena.k8s.eonerc.rwth-aachen.de |/|
|Arena|Server zu dem sich die Roboter verbinden können| arena.k8s.eonerc.rwth-aachen.de | 30352 |

## Ordner Struktur

- `common`: Definitionen, die allen Teilen des Praktikums, d.h. Roboter, Observer und Arena, bekannt sein müssen
- `robot`: Quellcode des Roboters Programms
- `script`: Aktuelle Version des verwendeten Skripts und der Hilfsblätter

## How-To
### Abhängigkeiten/Dependencies
Zum Kompilieren und Bauen der Roboter-Anwendungen wird ein entsprechender Compiler (`g++`), sowie das Build-Management-Tool (`make`) benötigt.
Unter Ubuntu können diese einfach über folgenden Befehl installiert werden:
```bash
sudo apt update && sudo apt install make g++
```

### Kompilieren/Bauen der Roboter-Anwendung
Öffnen Sie ein Terminal `<STRG>+<ALT>+T` und wechseln Sie mittels des "change directory"-Befehls (`cd`) in das Verzeichnis der Roboter-Anwendung.

```
cd path/to/robot
```

Die Roboter-Anwendung kann nun durch den Aufruf des Build-Management Tools `make` kompiliert und gebaut werden. Verwenden Sie dafür den Befehl
```
make all
```
oder die Kurzform
```
make
```

### Starten der Roboter-Anwendung
Wurde die Anwendung erfolgreich gebaut, so kann das Kommandozeilen-basierte Interface mittels
```
./robot
```
gestartet werden. Nach dem Start muss zunächst ein Name, sowie Adresse und Port der Arena angebeben werden. Um sich während der Versuchdurchführung das ständige Eingeben dieser Informationen zu sparen, können diese Informationen auch als zusätzliche Parameter beim Start der Anwendung übergeben werden. Verwenden Sie dafür den Befehel
```
./robot <name> <address> <port>
```
wobei Sie `<name>`, `<adress>` und `<port>` entsprechend ersetzen.
Durch die Verwendung der Pfeiltasten (`↑`|`↓`) können Sie im Terminal durch die zuletzt ausgeführten Befehle scrollen. So kann das erneute Eintippen dieses Funktionsaufrufs und der benötigten Informationen geschickt umgangen werden.

> **WICHTIG:** Für die Verbindung zwischen Roboter und Arena müssen Sie sich im Netzwerk der RWTH befinden (bspw. mittels des RWTH VPNs)!

## Troubleshooting
### Observer zeigt Arena/Roboter nicht korrekt an...

![](.images/observer-troubleshooting.png)

|||
|:-:|:-|
|**Fehler**| *SOCKET-ERROR: -1 (QAbstractSocket::SocketError)* <br> Der Observer lädt zwar, zeigt aber keinerlei Wände oder Roboter darin an.|
|**Ursache**| Verbindung zwischen Observer und Arena fehlerhaft.|
|**Lösungsansatz**| Prüfen ob korrekte Adresse der Arena gewählte wurde, siehe [Adressen](#adressen)|

### Roboter kann keine Verbindung zur Arena herstellen...

|||
|:-:|:-|
|**Fehler**| *ERROR: dialog() - send() returned -1* <br> Roboter wirft Fehler und wird nicht in der Arena dargestellt.|
|**Ursache**| Verbindung zwischen Roboter und Arena fehlerhaft.|
|**Lösungsansätze**| 1) Sicherstellen, dass Verbindung zum Internet besteht besteht<br>2) Sicherstellen, dass Sie sich im Netzwerk der RWTH befinden (z.B. mittels VPN)<br>3) Beim Starten des Roboters statt *arena.k8s.eonerc.rwth-aachen.de* eine der folgenden IPs verwenden: <br>        137.226.248.61<br>        137.226.248.62<br>        137.226.248.63|
> Beispiel: `./robot MeinRoboter 137.226.248.61 30352`

### Compiler nicht installiert
|||
|:-:|:-|
|**Fehler**| *make: g++: No such file or directory* <br> Roboter kann nicht gebaut werden und wirft einen Fehler beim Ausführen von `make`|
|**Ursache**| Der verwendete Compiler `g++` ist nicht (ordnungsgemäß) installiert.|
|**Lösungsansätze**| Sicherstellen, dass Compiler installiert ist. Siehe dazu [HowTo: Abhängigkeiten / Dependencies](#abhängigkeitendependencies)|

## Kontakt:
[![](.images/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

Felix Wege - fwege@eonerc.rwth-aachen.de
